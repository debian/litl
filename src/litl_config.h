#ifndef LITL_CONFIG_H
#define LITL_CONFIG_H


#define USE_GETTID 0


#define FORCE_32BIT 0

#if FORCE_32_BIT
/* compile for 32bit architecture */
#define HAVE_32BIT 1

#else  /* FORCE_32_BIT */
/* detect 32/64 bit architecture */

#ifndef SIZEOF_SIZE_T
#define SIZEOF_SIZE_T 8
#endif

#if (SIZEOF_SIZE_T == 4)
/* 32bit arch */
#define HAVE_32BIT 1
#else
/* 64bit arch */
#define HAVE_32BIT 0
#endif

#endif

#define CLOCK_GETTIME_AVAIL 1

#endif	/* LITL_CONFIG_H */
